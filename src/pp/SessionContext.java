package pp;

import pp.net.SessionHttpServer;

/**
 * Created by citan on 23.06.2016.
 */
public class SessionContext {
    private SessionHttpServer server;

    public SessionHttpServer getServer() {
        return server;
    }

    public void setServer(SessionHttpServer server) {
        this.server = server;
    }

    public boolean isSet() {
        return server != null;
    }

    public boolean isSlave() {
        return server.isSlaveServer();
    }
}
