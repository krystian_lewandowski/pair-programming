package pp.listener;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.LogicalPosition;
import com.intellij.openapi.editor.event.CaretEvent;
import com.intellij.openapi.editor.event.CaretListener;
import com.intellij.openapi.project.Project;
import pp.handler.EventHandler;
import pp.model.SessionCaretEvent;
import pp.net.client.SlaveSessionServer;
import pp.util.EventUtil;

import java.awt.*;
import java.io.IOException;

/**
 * Created by citan on 08.06.2016.
 */
public class CaretListenerProxy implements CaretListener {
    private final SlaveSessionServer server;
    private final Project project;
    private final EventHandler eventHandler;

    public CaretListenerProxy(SlaveSessionServer server, Project project, EventHandler eventHandler) {
        this.server = server;
        this.project = project;
        this.eventHandler = eventHandler;
    }

    @Override
    public void caretPositionChanged(CaretEvent e) {
        if (server.listenersDisabled()) {
            return;
        }

        Document document = e.getEditor().getDocument();

        SessionCaretEvent event = new SessionCaretEvent();
        event = (SessionCaretEvent) EventUtil.updateEvent(event, project, document);
        if (event == null) {
            return;
        }
        LogicalPosition oldPosition = e.getOldPosition();
        event.oldPosition = new Point(oldPosition.line, oldPosition.column);
        LogicalPosition newPosition = e.getNewPosition();
        event.newPosition = new Point(newPosition.line, newPosition.column);

        try {
            eventHandler.handleEvent(event);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void caretAdded(CaretEvent e) {

    }

    @Override
    public void caretRemoved(CaretEvent e) {

    }
}
