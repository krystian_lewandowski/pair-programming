package pp.listener;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.event.SelectionEvent;
import com.intellij.openapi.editor.event.SelectionListener;
import com.intellij.openapi.project.Project;
import pp.handler.EventHandler;
import pp.model.SessionDocumentEvent;
import pp.net.client.SlaveSessionServer;
import pp.util.EventUtil;

import java.io.IOException;

/**
 * Created by citan on 09.06.2016.
 */
public class SelectionListenerProxy implements SelectionListener {
    private final SlaveSessionServer server;
    private final Project project;
    private final EventHandler eventHandler;

    public SelectionListenerProxy(SlaveSessionServer server, Project project, EventHandler eventHandler) {
        this.server = server;
        this.project = project;
        this.eventHandler = eventHandler;
    }

    @Override
    public void selectionChanged(SelectionEvent e) {
        if (server.listenersDisabled()) {
            return;
        }
        Document document = e.getEditor().getDocument();

        SessionDocumentEvent event = EventUtil.updateEvent(new SessionDocumentEvent(), project, document);
        if (event == null) {
            return;
        }
        event.oldRanges = e.getOldRanges();
        event.newRanges = e.getNewRanges();

        try {
            eventHandler.handleEvent(event);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
