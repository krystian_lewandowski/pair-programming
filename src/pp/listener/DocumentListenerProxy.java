package pp.listener;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.event.DocumentEvent;
import com.intellij.openapi.editor.event.DocumentListener;
import com.intellij.openapi.project.Project;
import pp.handler.EventHandler;
import pp.model.SessionDocumentChangedEvent;
import pp.net.client.SlaveSessionServer;
import pp.util.EventUtil;
import pp.util.LogUtil;

import java.io.IOException;

import static com.intellij.codeInsight.completion.CompletionInitializationContext.DUMMY_IDENTIFIER_TRIMMED;

/**
 * Created by citan on 09.06.2016.
 */
public class DocumentListenerProxy implements DocumentListener {
    private final SlaveSessionServer server;
    private final Project project;
    private final EventHandler eventHandler;

    public DocumentListenerProxy(SlaveSessionServer server, Project project, EventHandler eventHandler) {
        this.server = server;
        this.project = project;
        this.eventHandler = eventHandler;
    }

    @Override
    public void beforeDocumentChange(DocumentEvent event) {

    }

    @Override
    public void documentChanged(DocumentEvent e) {
        LogUtil.info("handling document changed event");
        if (server.listenersDisabled()) {
            LogUtil.info("listener disabled");
            return;
        }
        String oldString = e.getOldFragment().toString();
        String newString = e.getNewFragment().toString();

        if (oldString.endsWith(DUMMY_IDENTIFIER_TRIMMED) || newString.endsWith(DUMMY_IDENTIFIER_TRIMMED)) {
            //string = string.substring(0, string.length() - DUMMY_IDENTIFIER_TRIMMED.length());
            return;
        }

        Document document = e.getDocument();
        SessionDocumentChangedEvent event = new SessionDocumentChangedEvent();
        event = (SessionDocumentChangedEvent) EventUtil.updateEvent(event, project, document);
        if (event == null) {
            return;
        }
        event.offset = e.getOffset();
        event.oldLength = e.getOldLength();
        event.newLength = e.getNewLength();
        event.oldFragment = e.getOldFragment().toString();
        event.newFragment = e.getNewFragment().toString();
        try {
            eventHandler.handleEvent(event);
        } catch (IOException ex) {
            LogUtil.error(ex);
        }
        LogUtil.info("handling document changed event: DONE");
    }
}
