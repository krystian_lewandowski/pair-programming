package pp.listener;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManagerListener;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.newvfs.BulkFileListener;
import com.intellij.openapi.vfs.newvfs.events.*;
import org.jetbrains.annotations.NotNull;
import pp.handler.EventHandler;
import pp.net.client.SlaveSessionServer;

import java.util.List;

/**
 * Created by citan on 08.06.2016.
 */
public class MessageBusListenerProxy implements BulkFileListener, FileDocumentManagerListener {

    private final SlaveSessionServer server;
    private final String basePath;
    private final EventHandler eventHandler;
    private final FileEditorManager manager;


    public MessageBusListenerProxy(SlaveSessionServer server, Project project, EventHandler eventHandler) {
        this.server = server;
        this.basePath = project.getBasePath();
        this.eventHandler = eventHandler;
        manager = FileEditorManager.getInstance(project);
    }

    @Override
    public void before(@NotNull List<? extends VFileEvent> events) {

    }

    @Override
    public void after(@NotNull List<? extends VFileEvent> events) {
        if (server.listenersDisabled()) {
            return;
        }

        for (VFileEvent event : events) {
            VirtualFile file = event.getFile();
            if (!isProjectMember(file)) {
                continue;
            }
            if (!hasValidEditor(file)) {
                continue;
            }

            if (event instanceof VFilePropertyChangeEvent) {

            } else if (event instanceof VFileCreateEvent) {

            } else if (event instanceof VFileMoveEvent) {

            } else if (event instanceof VFileDeleteEvent) {

            } else if (event instanceof VFileCreateEvent) {

            } else if (event instanceof VFileContentChangeEvent) {
                handleContentChange((VFileContentChangeEvent) event);
                FileEditor selectedEditor = manager.getSelectedEditor(file);
                if (selectedEditor instanceof TextEditor) {
                    TextEditor selectedEditor1 = (TextEditor) selectedEditor;
                    String text = selectedEditor1.getEditor().getDocument().getText();
                    System.out.print(true);
                }
            } else if (event instanceof VFileCopyEvent) {

            }
            // handle recursively
        }
    }

    private boolean isProjectMember(VirtualFile file) {
        return file.getPath().contains(basePath);
    }

    private boolean hasValidEditor(VirtualFile file) {
        return manager.getSelectedEditor(file) != null;
    }

    private void handleContentChange(VFileContentChangeEvent event) {


    }

    @Override
    public void beforeAllDocumentsSaving() {

    }

    @Override
    public void beforeDocumentSaving(@NotNull Document document) {

    }

    @Override
    public void beforeFileContentReload(VirtualFile file, @NotNull Document document) {

    }

    @Override
    public void fileWithNoDocumentChanged(@NotNull VirtualFile file) {

    }

    @Override
    public void fileContentReloaded(@NotNull VirtualFile file, @NotNull Document document) {

    }

    @Override
    public void fileContentLoaded(@NotNull VirtualFile file, @NotNull Document document) {

    }

    @Override
    public void unsavedDocumentsDropped() {

    }
}
