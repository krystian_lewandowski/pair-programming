package pp.net.client;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.sun.net.httpserver.HttpExchange;
import org.apache.commons.httpclient.HttpStatus;
import pp.model.SessionDocumentChangedEvent;
import pp.net.GsonHttpHandler;

/**
 * Created by citan on 29.06.2016.
 */
public class DocumentChangedHandler extends GsonHttpHandler<SessionDocumentChangedEvent> {
    private final SlaveSessionServer server;

    public DocumentChangedHandler(SlaveSessionServer server, Project project) {
        super(project, SessionDocumentChangedEvent.class, true);
        this.server = server;
    }

    @Override
    protected int handleEvent(HttpExchange httpExchange, final SessionDocumentChangedEvent event, final VirtualFile file) {
        if (event.documentPath == null || event.documentPath.length() == 0) {
            return HttpStatus.SC_NOT_FOUND;
        }

        server.setListenersDisabled(true);
        try {
            WriteCommandAction.runWriteCommandAction(project, new Runnable() {
                @Override
                public void run() {
                    FileEditorManager manager = FileEditorManager.getInstance(project);
                    FileEditor[] fileEditors;
                    if (manager.isFileOpen(file)) {
                        fileEditors = manager.getEditors(file);
                    } else {
                        fileEditors = manager.openFile(file, false, true);
                    }

                    if (fileEditors != null) {
                        for (FileEditor fileEditor : fileEditors) {
                            if (fileEditor instanceof TextEditor) {
                                modifyText(((TextEditor) fileEditor).getEditor(), event);
                            }
                        }
                    }
                }
            });
        } finally {
            server.setListenersDisabled(false);
        }
        return HttpStatus.SC_OK;
    }

    private void modifyText(Editor editor, SessionDocumentChangedEvent event) {
        Document document = editor.getDocument();
        document.replaceString(event.offset, event.offset + event.oldLength, event.newFragment);
    }
}
