package pp.net.client;

import com.google.gson.Gson;
import pp.handler.EventHandler;
import pp.model.SessionEvent;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by citan on 10.06.2016.
 */
public class SessionEventHandler implements EventHandler, Observer {

    private static ExecutorService outgoingExecutorService = Executors.newSingleThreadExecutor();
    private final Gson gson;
    private final String sessionAddress;

    public SessionEventHandler(String sessionAddress) {
        this.sessionAddress = sessionAddress;
        gson = new Gson();
    }

    public static void sendEvent(String sessionAddress, SessionEvent event, Observer observer, boolean slaveTarget) {
        outgoingExecutorService.submit(new OutgoingEventRunnable(sessionAddress, new Gson(), event, observer, slaveTarget));
    }

    public void start() {
//        outgoingExecutorService = Executors.newSingleThreadExecutor();
    }

    public void stop() {
//        outgoingExecutorService.shutdown();
    }

    @Override
    public void handleEvent(SessionEvent event) {
        outgoingExecutorService.submit(new OutgoingEventRunnable(sessionAddress, gson, event, this, false));
    }

    @Override
    public void update(Observable o, Object arg) {
        if (((int) arg) == -1) {

        }
    }
}
