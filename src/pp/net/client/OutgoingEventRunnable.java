package pp.net.client;

import com.google.gson.Gson;
import pp.model.SessionEvent;
import pp.net.NetworkConstants;
import pp.util.LogUtil;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Observer;

/**
 * Created by citan on 28.06.2016.
 */
class OutgoingEventRunnable implements Runnable {

    private final String sessionAddress;
    private final Gson gson;
    private final SessionEvent event;
    private final Observer observer;
    private final boolean slaveTarget;

    public OutgoingEventRunnable(String sessionAddress, Gson gson, SessionEvent event, Observer observer, boolean slaveTarget) {
        this.sessionAddress = sessionAddress;
        this.gson = gson;
        this.event = event;
        this.observer = observer;
        this.slaveTarget = slaveTarget;
    }

    @Override
    public void run() {
        int responseCode = -1;
        String value = gson.toJson(event);
        URL url = null;
        try {
            String className = event.getClass().getSimpleName();
            String targetPath = (slaveTarget) ? className + "Client" : className;

            url = new URL("http", sessionAddress, NetworkConstants.SESSION_HTTP_PORT, "/" + targetPath);
            LogUtil.info(value + " sending to: " + url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setConnectTimeout(3000);
            connection.setReadTimeout(3000);
            OutputStream outputStream = connection.getOutputStream();
            try {
                outputStream.write(value.getBytes("utf-8"));
            } finally {
                outputStream.close();
            }
            responseCode = connection.getResponseCode();
            LogUtil.info("response code: " + responseCode);

            connection.disconnect();
        } catch (IOException e) {
            responseCode = -1;
            LogUtil.info("Failed to make request: " + value + ", to: " + url + ", observer:" + observer);
            LogUtil.error(e);
        } finally {
            if (observer != null) {
                observer.update(null, responseCode);
            }
        }
    }
}
