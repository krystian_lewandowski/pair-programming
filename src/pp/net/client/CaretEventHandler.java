package pp.net.client;

import com.intellij.openapi.editor.*;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.awt.RelativePoint;
import com.sun.net.httpserver.HttpExchange;
import org.apache.commons.httpclient.HttpStatus;
import pp.model.SessionCaretEvent;
import pp.net.GsonHttpHandler;

import java.awt.*;

/**
 * Created by citan on 22.06.2016.
 */
class CaretEventHandler extends GsonHttpHandler<SessionCaretEvent> {
    private final SlaveSessionServer server;
    private final VirtualFile baseDir;
    private final Project project;
    private Balloon balloon;

    public CaretEventHandler(SlaveSessionServer server, Project project) {
        super(project, SessionCaretEvent.class, true);
        this.server = server;
        baseDir = project.getBaseDir();
        this.project = project;
    }

    @Override
    protected int handleEvent(HttpExchange httpExchange, final SessionCaretEvent event, final VirtualFile file) {
        server.setListenersDisabled(true);
        try {
            FileEditorManager manager = FileEditorManager.getInstance(project);
            FileEditor[] fileEditors;
            if (manager.isFileOpen(file)) {
                fileEditors = manager.getEditors(file);
            } else {
                fileEditors = manager.openFile(file, false, true);
            }

            if (fileEditors != null) {
                for (FileEditor fileEditor : fileEditors) {
                    if (fileEditor instanceof TextEditor) {
                        server.setListenersDisabled(true);
                        try {
                            setCaretPosition(((TextEditor) fileEditor).getEditor(), event);
                        } finally {
                            server.setListenersDisabled(false);
                        }
                    }
                }
            }
        } finally {
            server.setListenersDisabled(false);
        }

        return HttpStatus.SC_OK;
    }

    private void setCaretPosition(Editor editor, SessionCaretEvent event) {
        CaretModel caretModel = editor.getCaretModel();
        ScrollingModel scrollingModel = editor.getScrollingModel();

        LogicalPosition logicalPosition = new LogicalPosition(event.newPosition.x, event.newPosition.y);
        caretModel.moveToLogicalPosition(logicalPosition);
        logicalPosition = caretModel.getLogicalPosition();
        scrollingModel.disableAnimation();
        try {
            scrollingModel.scrollTo(logicalPosition, ScrollType.MAKE_VISIBLE);
        } finally {
            scrollingModel.enableAnimation();
        }

        Point p = calculatePoint(event, editor, logicalPosition);
        showBalloon(event, editor, p);
    }


    private Point calculatePoint(SessionCaretEvent event, Editor editor, LogicalPosition logicalPosition) {
//        CodeStyleSettings settings = CodeStyleSettingsManager.getSettings(project);
        //            VisualPosition visPos = new VisualPosition(editor.offsetToVisualPosition(editor.logicalPositionToOffset(logicalPosition)).line, settings.getRightMargin(null));

        int offset = editor.logicalPositionToOffset(logicalPosition);
        int line = editor.offsetToVisualPosition(offset).line + 1; // +1 line below
        int column = event.newPosition.y;
        return editor.visualPositionToXY(new VisualPosition(line, column));
    }

    private void showBalloon(SessionCaretEvent event, Editor editor, Point p) {
        final String htmlText = String.format("<div style=\"color:#000000; border-style:none; text-align: center; margin: 0px;\">%s</div>", event.name);

        if (balloon != null && !balloon.isDisposed()) {
            balloon.hide();
            balloon.dispose();
        }
        balloon = JBPopupFactory.getInstance()
                .createHtmlTextBalloonBuilder(htmlText, null, Color.WHITE, null)
                .setBorderInsets(new Insets(0, 0, 0, 0))
                .setFadeoutTime(5000)
                .createBalloon();
        balloon.setAnimationEnabled(false);
        balloon.show(new RelativePoint(editor.getContentComponent(), p), Balloon.Position.below);

    }

}
