package pp.net.client;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.event.EditorEventMulticaster;
import com.intellij.openapi.project.Project;
import com.intellij.util.messages.MessageBus;
import com.intellij.util.messages.MessageBusConnection;
import pp.listener.CaretListenerProxy;
import pp.listener.DocumentListenerProxy;
import pp.net.SessionHttpServer;

import java.io.IOException;

import static pp.net.NetworkConstants.CARET_EVENT_SLAVE_PATH;
import static pp.net.NetworkConstants.DOCUMENT_CHANGED_EVENT_SLAVE_PATH;

/**
 * Created by citan on 20.06.2016.
 */
public class SlaveSessionServer extends SessionHttpServer {
    protected final Project project;
    protected final String sessionAddress;

    private final EditorEventMulticaster eventMulticaster;
    private final MessageBus messageBus;
    private MessageBusConnection connection;

    private SessionEventHandler eventHandler;
    private CaretListenerProxy caretListener;
    //    private SelectionListenerProxy selectionListener;
//    private MessageBusListenerProxy messageBusListener;
    private DocumentListenerProxy documentListener;
    private boolean listenersDisabled;


    public SlaveSessionServer(Project project, String sessionAddress) {
        this.project = project;
        this.sessionAddress = sessionAddress;

        messageBus = ApplicationManager.getApplication().getMessageBus();
        eventMulticaster = EditorFactory.getInstance().getEventMulticaster();
    }

    @Override
    public void start() throws IOException {
        super.start();

        eventHandler = new SessionEventHandler(sessionAddress);
        eventHandler.start();

        createListeners();

        server.createContext(CARET_EVENT_SLAVE_PATH, new CaretEventHandler(this, project));
        server.createContext(DOCUMENT_CHANGED_EVENT_SLAVE_PATH, new DocumentChangedHandler(this, project));

        connection = messageBus.connect();
        registerListeners();
    }

    private void createListeners() {
        caretListener = new CaretListenerProxy(this, project, eventHandler);
//        selectionListener = new SelectionListenerProxy(this, project, eventHandler);
        documentListener = new DocumentListenerProxy(this, project, eventHandler);
//        messageBusListener = new MessageBusListenerProxy(this, project, eventHandler);
    }

    private void registerListeners() {
        eventMulticaster.addCaretListener(caretListener);
//        eventMulticaster.addSelectionListener(selectionListener);
        eventMulticaster.addDocumentListener(documentListener);

//        connection.subscribe(VirtualFileManager.VFS_CHANGES, messageBusListener);
//        connection.subscribe(AppTopics.FILE_DOCUMENT_SYNC, messageBusListener);
    }

    private void unregisterListeners() {
        eventMulticaster.removeCaretListener(caretListener);
//        eventMulticaster.removeSelectionListener(selectionListener);
        eventMulticaster.removeDocumentListener(documentListener);

        caretListener = null;
//        selectionListener = null;
        documentListener = null;
    }

    private void disposeListeners() {
        caretListener = null;
    }

    public void stop() {
        super.stop();

        unregisterListeners();
        connection.disconnect();

        disposeListeners();
        eventHandler.stop();
        eventHandler = null;
    }

    @Override
    public boolean isSlaveServer() {
        return true;
    }

    public void setListenersDisabled(boolean listenersDisabled) {
        this.listenersDisabled = listenersDisabled;
    }

    public boolean listenersDisabled() {
        return listenersDisabled;
    }
}
