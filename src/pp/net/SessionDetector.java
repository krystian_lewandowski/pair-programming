package pp.net;

import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import pp.util.CIDRUtils;
import pp.util.NetworkUtil;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Observer;

import static pp.net.NetworkConstants.*;

/**
 * Created by citan on 15.06.2016.
 */
public class SessionDetector implements Runnable {

    private final Application application;
    private final Observer observer;
    private DatagramSocket sessionDetectorRecvSocket;

    public SessionDetector(Observer observer) {
        this.observer = observer;
        application = ApplicationManager.getApplication();

    }

    public void detectRunningSessions() throws IOException {
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            if (networkInterfaces != null) {
                while (networkInterfaces.hasMoreElements()) {
                    NetworkInterface networkInterface = networkInterfaces.nextElement();
                    if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                        continue;
                    }

                    List<InterfaceAddress> interfaceAddresses = networkInterface.getInterfaceAddresses();
                    for (InterfaceAddress interfaceAddress : interfaceAddresses) {
                        String hostAddress = interfaceAddress.getAddress().getHostAddress();
                        short networkPrefixLength = interfaceAddress.getNetworkPrefixLength();
                        CIDRUtils cidrUtils = new CIDRUtils(hostAddress + "/" + networkPrefixLength);
                        String broadcastAddressString = cidrUtils.getBroadcastAddress();
                        InetAddress broadcastAddress = InetAddress.getByName(broadcastAddressString);
                        socket.setBroadcast(true);
                        try {
                            NetworkUtil.sentDatagramPacket(socket, DISCOVER_ACTIVE_SESSIONS_BYTES, broadcastAddress, ACTIVE_SESSION_RECV_PORT);
                        } catch (IOException e) {
                            // ignore
                        }
                    }
                }
            }
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    public void start() throws SocketException {
        sessionDetectorRecvSocket = new DatagramSocket(SESSION_DETECTOR_RECV_PORT);
        sessionDetectorRecvSocket.setSoTimeout(RECV_TIMEOUT);
        application.executeOnPooledThread(this);
    }

    public void stop() {
        sessionDetectorRecvSocket.close();
    }

    @Override
    public void run() {
        byte[] recvData = new byte[PACKET_SIZE];
        DatagramPacket packet = new DatagramPacket(recvData, recvData.length);

        while (!sessionDetectorRecvSocket.isClosed()) {
            try {
                sessionDetectorRecvSocket.receive(packet);
                if (Arrays.equals(SESSION_ACTIVE_BYTES, NetworkUtil.getBytes(packet))) {
                    updateObserver(packet);
                }
            } catch (IOException e) {
                // ignore
            }
        }
        updateObserver(null);
    }

    private void updateObserver(DatagramPacket packet) {
        final Object arg = (packet != null) ? packet.getAddress() : null;
        application.invokeLater(new Runnable() {
            @Override
            public void run() {
                observer.update(null, arg);
            }
        });
    }
}
