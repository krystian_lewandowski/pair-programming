package pp.net.server;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import pp.model.*;
import pp.net.NetworkConstants;
import pp.net.client.SlaveSessionServer;
import pp.util.LogUtil;
import pp.util.NetworkUtil;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Arrays;

import static pp.net.NetworkConstants.*;

/**
 * Created by citan on 16.06.2016.
 */
public class MasterSessionServer extends SlaveSessionServer implements Runnable {
    private final ClientsListModel model;

    private DatagramSocket sessionServerRecvSocket;

    public MasterSessionServer(Project project, ClientsListModel model) {
        super(project, LOCAL_SESSION_HOST);
        this.model = model;
    }

    public void start() throws IOException {
        super.start();

        server.createContext(JOIN_EVENT_PATH, new JoinEventHandler(null, this));
        server.createContext(CARET_EVENT_MASTER_PATH, new MasterEventHandler(project, SessionCaretEvent.class, model));
        server.createContext(DOCUMENT_CHANGED_EVENT_MASTER_PATH, new MasterEventHandler(project, SessionDocumentChangedEvent.class, model));

        sessionServerRecvSocket = new DatagramSocket(NetworkConstants.ACTIVE_SESSION_RECV_PORT);
        ApplicationManager.getApplication().executeOnPooledThread(this);
    }

    public void stop() {
        super.stop();
        sessionServerRecvSocket.close();
        this.model.clear();
    }

    @Override
    public void run() {
        byte[] recvData = new byte[PACKET_SIZE];
        DatagramPacket packet = new DatagramPacket(recvData, recvData.length);

        while (!sessionServerRecvSocket.isClosed()) {
            try {
                sessionServerRecvSocket.receive(packet);
                if (Arrays.equals(DISCOVER_ACTIVE_SESSIONS_BYTES, NetworkUtil.getBytes(packet))) {
                    NetworkUtil.sentDatagramPacket(sessionServerRecvSocket, SESSION_ACTIVE_BYTES, packet.getAddress(), SESSION_DETECTOR_RECV_PORT);
                }
            } catch (IOException e) {
                // ignore
            }
        }
    }

    public void clientJoined(SessionJoinEvent event, String address) {
        LogUtil.info("client joined: " + event.name + ", address: " + address);
        model.add(new Client(event, address));
    }

    @Override
    public boolean isSlaveServer() {
        return false;
    }
}
