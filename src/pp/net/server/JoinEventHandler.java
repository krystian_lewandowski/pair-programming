package pp.net.server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.sun.net.httpserver.HttpExchange;
import org.apache.commons.httpclient.HttpStatus;
import pp.model.SessionJoinEvent;
import pp.net.GsonHttpHandler;

/**
 * Created by citan on 22.06.2016.
 */
public class JoinEventHandler extends GsonHttpHandler<SessionJoinEvent> {
    private final MasterSessionServer sessionServer;

    public JoinEventHandler(Project project, MasterSessionServer sessionServer) {
        super(project, SessionJoinEvent.class, true);
        this.sessionServer = sessionServer;
    }

    @Override
    protected int handleEvent(HttpExchange httpExchange, final SessionJoinEvent event, VirtualFile file) {
        final String address = httpExchange.getRemoteAddress().getAddress().getHostAddress();
        sessionServer.clientJoined(event, address);
        return HttpStatus.SC_OK;
    }
}
