package pp.net.server;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.sun.net.httpserver.HttpExchange;
import org.apache.commons.httpclient.HttpStatus;
import pp.model.Client;
import pp.model.ClientsListModel;
import pp.model.SessionEvent;
import pp.net.GsonHttpHandler;
import pp.net.client.SessionEventHandler;

import java.util.List;

/**
 * Created by citan on 24.06.2016.
 */
public class MasterEventHandler<T> extends GsonHttpHandler {
    private final ClientsListModel model;

    public MasterEventHandler(Project project, Class<T> clazz, ClientsListModel model) {
        super(project, clazz, false);
        this.model = model;
    }

    @Override
    protected int handleEvent(HttpExchange httpExchange, SessionEvent event, VirtualFile file) {
        String sourceAddress = httpExchange.getRemoteAddress().getAddress().getHostAddress();
        List<Client> clients = model.copy();

        for (Client client : clients) {
            if (client.getAddress().equals(sourceAddress)) {
                continue;
            }
            SessionEventHandler.sendEvent(client.getAddress(), event, null, true);
        }
        return HttpStatus.SC_OK;
    }
}
