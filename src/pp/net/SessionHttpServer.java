package pp.net;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by citan on 20.06.2016.
 */
public abstract class SessionHttpServer {
    protected HttpServer server;
    private boolean running;
    private ExecutorService outgoingExecutorService;

    public SessionHttpServer() {
        this.outgoingExecutorService = Executors.newSingleThreadExecutor();
    }

    public void start() throws IOException {
        try {
            server = HttpServer.create(new InetSocketAddress(NetworkConstants.SESSION_HTTP_PORT), 0);
            server.setExecutor(Executors.newSingleThreadExecutor()); // creates a default executor
            server.start();
            running = true;
        } catch (IOException e) {
            if (server != null) {
                stop();
            }
            throw e;
        }
    }

    public void stop() {
        running = true;
        server.stop(0);
    }

    public abstract boolean isSlaveServer();

    public boolean isRunning() {
        return running;
    }
}
