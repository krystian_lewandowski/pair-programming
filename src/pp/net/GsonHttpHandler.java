package pp.net;

import com.google.gson.Gson;
import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.ModalityState;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.httpclient.HttpStatus;
import pp.model.SessionDocumentEvent;
import pp.model.SessionEvent;
import pp.util.LogUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by citan on 22.06.2016.
 */
public abstract class GsonHttpHandler<T extends SessionEvent> implements HttpHandler {
    protected final Project project;
    private final Gson gson;
    private final Class<T> clazz;
    private final boolean handleInMainThread;
    private final Application application;
    private VirtualFile baseDir;

    public GsonHttpHandler(Project project, Class<T> clazz, boolean handleInMainThread) {
        this.project = project;
        this.gson = new Gson();
        this.clazz = clazz;
        this.handleInMainThread = handleInMainThread;
        if (project != null) {
            baseDir = project.getBaseDir();
        }
        application = ApplicationManager.getApplication();
    }

    private int handleEventInternally(HttpExchange httpExchange, T event, VirtualFile file) {
        if (handleInMainThread) {
            BlockingRunnable blockingRunnable = new BlockingRunnable(httpExchange, event, file);
            application.invokeAndWait(blockingRunnable, ModalityState.NON_MODAL);
            return blockingRunnable.getStatusCode();
        } else {
            return handleEvent(httpExchange, event, file);
        }
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        int responseCode = HttpStatus.SC_OK;
        InputStreamReader isr = new InputStreamReader(httpExchange.getRequestBody(), "utf-8");
        BufferedReader br = new BufferedReader(isr);
        try {
            String value = br.readLine();
            LogUtil.info(this.getClass().getSimpleName() + ": handling event:" + value);
            T event = gson.fromJson(value, clazz);

            VirtualFile file = null;
            if (event instanceof SessionDocumentEvent) {
                SessionDocumentEvent documentEvent = (SessionDocumentEvent) event;
                if (documentEvent.documentPath != null) {
                    file = baseDir.findFileByRelativePath(documentEvent.documentPath);
                    if (file != null) {
                        responseCode = handleEventInternally(httpExchange, event, file);
                    }
                } else {
                    responseCode = HttpStatus.SC_NOT_FOUND;
                }
            } else {
                responseCode = handleEventInternally(httpExchange, event, file);
            }
        } catch (Throwable t) {
            LogUtil.error(t);
        } finally {
            br.close();
        }

        httpExchange.sendResponseHeaders(responseCode, 0);
    }

    protected abstract int handleEvent(HttpExchange httpExchange, T event, VirtualFile file);


    private class BlockingRunnable implements Runnable {
        private final HttpExchange httpExchange;
        private final T event;
        private final VirtualFile file;
        private int statusCode;

        public BlockingRunnable(HttpExchange httpExchange, T event, VirtualFile file) {
            this.httpExchange = httpExchange;
            this.event = event;
            this.file = file;
        }

        @Override
        public void run() {
            statusCode = handleEvent(httpExchange, event, file);
        }

        public int getStatusCode() {
            return statusCode;
        }
    }
}
