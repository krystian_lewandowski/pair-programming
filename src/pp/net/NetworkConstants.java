package pp.net;

import pp.model.SessionCaretEvent;
import pp.model.SessionDocumentChangedEvent;
import pp.model.SessionJoinEvent;

/**
 * Created by citan on 16.06.2016.
 */
public class NetworkConstants {
    public static final int RECV_TIMEOUT = 300;
    public static final int PACKET_SIZE = 64;
    public static final int SESSION_DETECTOR_RECV_PORT = 63345;
    public static final int ACTIVE_SESSION_RECV_PORT = 63346;
    public static final int SESSION_HTTP_PORT = 63347;

    public static final String LOCAL_SESSION_HOST = "localhost";

    public static final byte[] DISCOVER_ACTIVE_SESSIONS_BYTES = "DISCOVER ACTIVE SESSIONS".getBytes();
    public static final byte[] SESSION_ACTIVE_BYTES = "SESSION ACTIVE".getBytes();

    public static final String CARET_EVENT_MASTER_PATH = "/" + SessionCaretEvent.class.getSimpleName();
    public static final String CARET_EVENT_SLAVE_PATH = "/" + SessionCaretEvent.class.getSimpleName() + "Client";
    public static final String DOCUMENT_CHANGED_EVENT_MASTER_PATH = "/" + SessionDocumentChangedEvent.class.getSimpleName();
    public static final String DOCUMENT_CHANGED_EVENT_SLAVE_PATH = "/" + SessionDocumentChangedEvent.class.getSimpleName() + "Client";
    public static final String JOIN_EVENT_PATH = "/" + SessionJoinEvent.class.getSimpleName();


}
