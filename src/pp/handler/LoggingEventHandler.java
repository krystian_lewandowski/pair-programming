package pp.handler;

import com.google.gson.Gson;
import com.intellij.openapi.diagnostic.Logger;
import pp.model.SessionEvent;

/**
 * Created by citan on 08.06.2016.
 */
public class LoggingEventHandler implements EventHandler {

    private static final Logger LOG = Logger.getInstance(LoggingEventHandler.class);
    private final Gson gson;

    public LoggingEventHandler() {
        gson = new Gson();
    }


    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void handleEvent(SessionEvent event) {
        if (event != null) {
            LOG.info(gson.toJson(event));
        }
    }
}
