package pp.handler;

import pp.model.SessionEvent;

import java.io.IOException;

/**
 * Created by citan on 08.06.2016.
 */
public interface EventHandler {

    void start();

    void stop();

    void handleEvent(SessionEvent event) throws IOException;
}
