package pp.util;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.vcsUtil.VcsFileUtil;
import pp.model.SessionDocumentEvent;

/**
 * Created by citan on 09.06.2016.
 */
public class EventUtil {

    public static SessionDocumentEvent updateEvent(SessionDocumentEvent event, Project project, Document document) {
        event.name = System.getProperty("user.name");
        VirtualFile virtualFile = FileDocumentManager.getInstance().getFile(document);
        if (virtualFile != null) {
            VirtualFile baseDir = project.getBaseDir();
            if (FileUtil.isFilePartOfProject(project, virtualFile)) {
                event.documentPath = VcsFileUtil.relativePath(baseDir, virtualFile);
                if (event.documentPath == null || event.documentPath.length() == 0) {
                    LogUtil.info("updateEvent: " + virtualFile + " - empty string or returned for relative path");
                    return null;
                }
                event.documentHashCode = document.getText().hashCode();
            } else {
                return null;
            }
        }
        return event;
    }
}
