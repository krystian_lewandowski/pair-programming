package pp.util;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by citan on 16.06.2016.
 */
public class NetworkUtil {

    public static byte[] getBytes(DatagramPacket packet) {
        return Arrays.copyOf(packet.getData(), packet.getLength());
    }

    public static void sentDatagramPacket(DatagramSocket socket, byte[] data, InetAddress address, int port) throws IOException {
        socket.send(new DatagramPacket(data, data.length, address, port));
    }

    public static boolean isLocalInterface(String address) throws SocketException {
        boolean isLocal = false;
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        if (networkInterfaces != null) {
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = networkInterfaces.nextElement();

                List<InterfaceAddress> interfaceAddresses = networkInterface.getInterfaceAddresses();
                for (InterfaceAddress interfaceAddress : interfaceAddresses) {
                    String hostAddress = interfaceAddress.getAddress().getHostAddress();
                    if (hostAddress.equals(address)) {
                        isLocal = true;
                        break;
                    }
                }
            }
        }
        return isLocal;
    }
}
