package pp.util;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.vcsUtil.VcsFileUtil;

/**
 * Created by citan on 10.06.2016.
 */
public class FileUtil {

    public static boolean isFilePartOfProject(Project project, VirtualFile file) {
        VirtualFile baseDir = project.getBaseDir();
        String relativePath = VcsFileUtil.relativePath(baseDir, file);
        return baseDir.findFileByRelativePath(relativePath) != null;
    }
}
