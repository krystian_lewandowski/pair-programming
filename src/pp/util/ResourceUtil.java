package pp.util;

import java.util.ResourceBundle;

/**
 * Created by citan on 16.06.2016.
 */
public class ResourceUtil {
    private static ResourceBundle bundle =
            ResourceBundle.getBundle("pp");

    public static String get(String key) {
        return bundle.getString(key);
    }
}
