package pp.util;

import com.intellij.openapi.diagnostic.Logger;

/**
 * Created by citan on 11.06.2016.
 */
public class LogUtil {
    private static final Logger LOG = Logger.getInstance(LogUtil.class);

    public static void info(String message) {
        LOG.info(message);
    }

    public static void error(Throwable t) {
        LOG.error(t);
    }
}
