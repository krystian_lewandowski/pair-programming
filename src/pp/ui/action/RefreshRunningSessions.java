package pp.ui.action;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.DumbAwareAction;
import pp.model.Session;
import pp.model.SessionsListModel;
import pp.ui.RunningSessionsListController;
import pp.util.ResourceUtil;

import java.net.InetAddress;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by citan on 15.06.2016.
 */
public class RefreshRunningSessions extends DumbAwareAction implements Observer {
    private final RunningSessionsListController runningSessionsListController;
    private final SessionsListModel sessionsListModel;
    private boolean isRunning;


    public RefreshRunningSessions(RunningSessionsListController runningSessionsListController) {
        super(ResourceUtil.get("discover_sessions_title"), ResourceUtil.get("discover_sessions_desc"), AllIcons.Actions.Refresh);
        this.runningSessionsListController = runningSessionsListController;
        sessionsListModel = runningSessionsListController.getSessionsListModel();
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        isRunning = true;
        sessionsListModel.clear();
        runningSessionsListController.refresh(this);
        updateEnabledState(e);
    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
        updateEnabledState(e);
    }

    private void updateEnabledState(AnActionEvent e) {
        e.getPresentation().setEnabled(!isRunning);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg == null) {
            isRunning = false;
        } else {
            InetAddress address = (InetAddress) arg;
            Session session = new Session(null, address);
            sessionsListModel.add(session);
        }
    }
}
