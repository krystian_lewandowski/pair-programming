package pp.ui.action;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import org.apache.commons.httpclient.HttpStatus;
import pp.SessionContext;
import pp.model.ClientsListModel;
import pp.model.SessionJoinEvent;
import pp.net.NetworkConstants;
import pp.net.SessionHttpServer;
import pp.net.client.SessionEventHandler;
import pp.net.server.MasterSessionServer;
import pp.util.LogUtil;
import pp.util.ResourceUtil;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by citan on 15.06.2016.
 */
public class StartSession extends DumbAwareAction implements Observer {

    private final ClientsListModel model;
    private final SessionContext sessionContext;
    private final Application application;

    public StartSession(SessionContext sessionContext, ClientsListModel model) {
        super(ResourceUtil.get("start_session_title"), ResourceUtil.get("start_session_desc"), AllIcons.General.Run);
        this.sessionContext = sessionContext;
        this.model = model;
        application = ApplicationManager.getApplication();
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        if (sessionContext.isSet() && sessionContext.isSlave()) {
            updatePresentation(e);
            return;
        }

        Project project = DataKeys.PROJECT.getData(e.getDataContext());
        try {
            if (sessionContext.isSet()) {
                stopSession();
            } else {
                startSession(project);
            }
        } catch (IOException ex) {
            e.getPresentation().setEnabled(true);
            LogUtil.error(ex);
        }
        updatePresentation(e);
    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
        updatePresentation(e);
    }

    private void startSession(Project project) throws IOException {
        SessionHttpServer sessionServer = new MasterSessionServer(project, model);
        sessionServer.start();
        sessionContext.setServer(sessionServer);
        SessionJoinEvent sessionJoinEvent = new SessionJoinEvent();
        sessionJoinEvent.name = System.getProperty("user.name");
        // TODO handle join event
        SessionEventHandler.sendEvent(NetworkConstants.LOCAL_SESSION_HOST, sessionJoinEvent, this, false);
    }

    private void stopSession() {
        SessionHttpServer sessionServer = sessionContext.getServer();
        if (sessionServer != null) {
            sessionServer.stop();
            sessionContext.setServer(null);
        }
    }

    private void updatePresentation(AnActionEvent e) {
        Presentation presentation = e.getPresentation();
        presentation.setEnabled(true);
        if (!sessionContext.isSet()) {
            presentation.setIcon(AllIcons.Actions.Execute);
            presentation.setText(ResourceUtil.get("start_session_title"));
            presentation.setDescription(ResourceUtil.get("start_session_desc"));
        } else if (!sessionContext.isSlave()) {
            presentation.setIcon(AllIcons.Actions.Suspend);
            presentation.setText(ResourceUtil.get("stop_session_title"));
            presentation.setDescription(ResourceUtil.get("stop_session_desc"));
        } else {
            presentation.setEnabled(false);
        }

    }

    @Override
    public void update(Observable o, final Object arg) {
        application.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (((int) arg) != HttpStatus.SC_OK) {
                    stopSession();
                }
            }
        });
    }
}
