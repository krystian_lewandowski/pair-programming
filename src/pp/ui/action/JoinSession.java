package pp.ui.action;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.DumbAwareAction;
import com.intellij.openapi.project.Project;
import org.apache.commons.httpclient.HttpStatus;
import pp.SessionContext;
import pp.model.Session;
import pp.model.SessionJoinEvent;
import pp.net.SessionHttpServer;
import pp.net.client.SessionEventHandler;
import pp.net.client.SlaveSessionServer;
import pp.util.LogUtil;
import pp.util.ResourceUtil;

import javax.swing.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by citan on 15.06.2016.
 */
public class JoinSession extends DumbAwareAction implements Observer {

    private final SessionContext sessionContext;
    private final JList<Session> availableSessionsList;
    private final JTextField sessionAddress;
    private final Application application;

    public JoinSession(SessionContext sessionContext, JList<Session> availableSessionsList, JTextField sessionAddress) {
        super(ResourceUtil.get("join_session_title"), ResourceUtil.get("join_session_desc"), AllIcons.Actions.Share);
        this.sessionContext = sessionContext;
        this.availableSessionsList = availableSessionsList;
        this.sessionAddress = sessionAddress;
        application = ApplicationManager.getApplication();
    }

    @Override
    public void actionPerformed(AnActionEvent e) {
        if (sessionContext.isSet() && !sessionContext.isSlave()) {
            updatePresentation(e);
            return;
        }

        if (!sessionContext.isSet()) {
            Project project = DataKeys.PROJECT.getData(e.getDataContext());
            int selectedIndex = availableSessionsList.getSelectedIndex();
            Session session = null;
            if (selectedIndex < 0) {
                String sessionAddressValue = sessionAddress.getText();
                if (sessionAddressValue != null && sessionAddressValue.length() > 0) {
                    try {
                        session = new Session("manual", InetAddress.getByName(sessionAddressValue));
                    } catch (UnknownHostException ex) {
                        LogUtil.error(ex);
                    }
                }
            } else {
                session = availableSessionsList.getModel().getElementAt(selectedIndex);
            }

            if (session != null) {
                startSession(project, session);
            }
        } else {
            stopSession();
        }
        updatePresentation(e);
    }

    private void startSession(Project project, Session session) {
        SlaveSessionServer slaveSessionServer = new SlaveSessionServer(project, session.getAddress());
        sessionContext.setServer(slaveSessionServer);
        SessionJoinEvent sessionJoinEvent = new SessionJoinEvent();
        sessionJoinEvent.name = System.getProperty("user.name");
        SessionEventHandler.sendEvent(session.getAddress(), sessionJoinEvent, this, false);
    }

    private void stopSession() {
        SessionHttpServer server = sessionContext.getServer();
        server.stop();
        sessionContext.setServer(null);
    }

    @Override
    public void update(AnActionEvent e) {
        super.update(e);
        updatePresentation(e);
    }

    private void updatePresentation(AnActionEvent e) {
        Presentation presentation = e.getPresentation();
        presentation.setEnabled(true);
        availableSessionsList.setEnabled(true);

        if (!sessionContext.isSet()) {
            presentation.setIcon(AllIcons.Actions.Share);
            presentation.setText(ResourceUtil.get("join_session_title"));
            presentation.setDescription(ResourceUtil.get("join_session_desc"));
        } else if (sessionContext.isSlave()) {
            availableSessionsList.setEnabled(false);
            presentation.setIcon(AllIcons.Actions.Unshare);
            presentation.setText(ResourceUtil.get("disconnect_session_title"));
            presentation.setDescription(ResourceUtil.get("disconnect_session_desc"));
        } else {
            availableSessionsList.setEnabled(false);
            presentation.setEnabled(false);
        }
    }

    @Override
    public void update(Observable o, final Object arg) {
        application.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (((int) arg) == HttpStatus.SC_OK) {
                    try {
                        sessionContext.getServer().start();
                    } catch (IOException e) {
                        LogUtil.error(e);
                        stopSession();
                    }
                } else {
                    stopSession();
                }
            }
        });
    }
}
