package pp.ui;

import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.ActionPlaces;
import com.intellij.openapi.actionSystem.ActionToolbar;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.SimpleToolWindowPanel;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import org.jetbrains.annotations.NotNull;
import pp.SessionContext;
import pp.model.Client;
import pp.model.ClientsListModel;
import pp.model.Session;
import pp.ui.action.JoinSession;
import pp.ui.action.RefreshRunningSessions;
import pp.ui.action.StartSession;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by citan on 11.06.2016.
 */
public class ClientsToolWindowFactory implements ToolWindowFactory, Observer {

    public static final String PP_ACTION_START_SESSION = "pp.action.StartSession";
    public static final String PP_ACTION_JOIN_SESSION = "pp.action.JoinSession";
    public static final String PP_ACTION_REFRESH_SESSIONS_LIST = "pp.action.RefreshSessionsList";
    private JPanel mainPanel;
    private JList<Session> availableSessionsList;
    private JList<Client> currentSessionClientsList;
    private JTextField sessionAddress;
    private JButton refreshButton;

    private SessionContext sessionContext;


    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        sessionContext = new SessionContext();

        SimpleToolWindowPanel panel = new SimpleToolWindowPanel(false, true);

        final Content content = ContentFactory.SERVICE.getInstance().createContent(panel, "", false);
        content.setCloseable(true);

        RunningSessionsListController runningSessionsListController = new RunningSessionsListController();

        setUpMainPanel(runningSessionsListController);
        panel.setContent(mainPanel);

        ActionToolbar actionToolbar = setUpActions(runningSessionsListController);
        actionToolbar.setTargetComponent(panel);
        panel.setToolbar(actionToolbar.getComponent());

        toolWindow.getContentManager().addContent(content);
    }

    private ActionToolbar setUpActions(RunningSessionsListController runningSessionsListController) {
        DefaultActionGroup group = new DefaultActionGroup();
        ActionManager actionManager = ActionManager.getInstance();
        actionManager.registerAction(PP_ACTION_START_SESSION, new StartSession(sessionContext, (ClientsListModel) currentSessionClientsList.getModel()));
        group.add(actionManager.getAction(PP_ACTION_START_SESSION));
        actionManager.registerAction(PP_ACTION_JOIN_SESSION, new JoinSession(sessionContext, availableSessionsList, sessionAddress));
        group.add(actionManager.getAction(PP_ACTION_JOIN_SESSION));
        actionManager.registerAction(PP_ACTION_REFRESH_SESSIONS_LIST, new RefreshRunningSessions(runningSessionsListController));
        group.add(actionManager.getAction(PP_ACTION_REFRESH_SESSIONS_LIST));
        return ActionManager.getInstance().createActionToolbar(ActionPlaces.UNKNOWN, group, false);
    }

    private void setUpMainPanel(RunningSessionsListController runningSessionsListController) {
        availableSessionsList.setModel(runningSessionsListController.getSessionsListModel());
        currentSessionClientsList.setModel(new ClientsListModel());
        currentSessionClientsList.setEnabled(false);
    }

    @Override
    public void update(Observable o, Object arg) {
        ApplicationManager.getApplication().invokeLater(new Runnable() {
            @Override
            public void run() {
                refreshButton.setEnabled(true);
            }
        });
    }
}
