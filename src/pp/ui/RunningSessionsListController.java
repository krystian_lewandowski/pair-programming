package pp.ui;

import com.intellij.openapi.application.ApplicationManager;
import pp.model.SessionsListModel;
import pp.net.SessionDetector;
import pp.util.LogUtil;

import java.io.IOException;
import java.util.Observer;

/**
 * Created by citan on 11.06.2016.
 */
public class RunningSessionsListController {

    private final SessionsListModel sessionsListModel;

    public RunningSessionsListController() {
        sessionsListModel = new SessionsListModel();
    }

    public SessionsListModel getSessionsListModel() {
        return sessionsListModel;
    }

    public void refresh(final Observer observer) {
        ApplicationManager.getApplication().executeOnPooledThread(new Runnable() {
            @Override
            public void run() {
                SessionDetector sessionDetector = null;
                try {
                    sessionDetector = new SessionDetector(observer);
                    sessionDetector.start();
                    sessionDetector.detectRunningSessions();
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    LogUtil.error(e);
                } catch (IOException e) {
                    LogUtil.error(e);
                } finally {
                    if (sessionDetector != null) {
                        sessionDetector.stop();
                    }
                }
            }
        });
    }
}
