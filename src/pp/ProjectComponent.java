package pp;

import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

/**
 * Created by citan on 08.06.2016.
 */
public class ProjectComponent implements com.intellij.openapi.components.ProjectComponent {

    public ProjectComponent(Project project) {

    }

    @Override
    public void projectOpened() {

    }

    @Override
    public void projectClosed() {
    }

    @Override
    public void initComponent() {
    }

    @Override
    public void disposeComponent() {
    }

    @NotNull
    @Override
    public String getComponentName() {
        return getClass().getName();
    }


    //        FileEditorManager manager = FileEditorManager.getInstance(project);
//        manager.getAllEditors()
//        VirtualFile virtualFile = FileDocumentManager.getInstance().getFile(document);
//        manager.openEditor()
    //manager.openFile(virtualFile, false, true);
    //LocalFileSystem.getInstance().findFileByPath()
    //http://www.jetbrains.org/intellij/sdk/docs/tutorials/editor_basics/working_with_text.html


}
