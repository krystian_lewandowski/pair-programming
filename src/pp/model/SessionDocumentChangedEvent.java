package pp.model;

/**
 * Created by citan on 29.06.2016.
 */
public class SessionDocumentChangedEvent extends SessionDocumentEvent {
    public int offset;
    public int oldLength;
    public int newLength;
    public String oldFragment;
    public String newFragment;

}
