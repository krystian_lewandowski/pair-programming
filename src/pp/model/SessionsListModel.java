package pp.model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by citan on 11.06.2016.
 */
public class SessionsListModel extends AbstractListModel {
    List<Session> data;

    public SessionsListModel() {
        data = new ArrayList<>();
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Object getElementAt(int index) {
        return data.get(index);
    }

    public void clear() {
        int last = data.size() - 1;
        data.clear();
        fireContentsChanged(this, 0, last);
    }

    public boolean add(Session session) {
        boolean added = false;
        if (!data.contains(session)) {
            data.add(session);
            int last = data.size() - 1;
            fireContentsChanged(this, last, last);
            added = true;
        }
        return added;
    }
}
