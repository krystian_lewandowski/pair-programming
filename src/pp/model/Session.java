package pp.model;

import java.net.InetAddress;

/**
 * Created by citan on 11.06.2016.
 */
public class Session {
    private String name;
    private String address;

    public Session(String name, InetAddress address) {
        this.name = name;
        this.address = address.getHostName();
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return name != null ? (name + ", ") : "" + address;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Session) {
            Session session = (Session) obj;
            return toString().equals(session.toString());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
