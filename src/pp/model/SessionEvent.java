package pp.model;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by citan on 09.06.2016.
 */
public class SessionEvent {
    private static AtomicInteger eventNumberHolder = new AtomicInteger(0);
    public int id = eventNumberHolder.getAndIncrement();
    public String name;
}
