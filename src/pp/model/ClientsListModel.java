package pp.model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by citan on 22.06.2016.
 */
public class ClientsListModel extends AbstractListModel {
    List<Client> data;

    public ClientsListModel() {
        data = new ArrayList<>();
    }

    @Override
    public int getSize() {
        return data.size();
    }

    @Override
    public Object getElementAt(int index) {
        return data.get(index);
    }

    public synchronized void clear() {
        int last = data.size() - 1;
        data.clear();
        fireContentsChanged(this, 0, last);
    }

    public synchronized boolean add(Client client) {
        boolean added = false;
        if (!data.contains(client)) {
            data.add(client);
            int last = data.size() - 1;
            fireContentsChanged(this, last, last);
            added = true;
        }
        return added;
    }

    public synchronized List<Client> copy() {
        return new ArrayList<>(data);
    }
}
