package pp.model;

/**
 * Created by citan on 22.06.2016.
 */
public class Client {
    private final String name;
    private final String address;

    public Client(SessionJoinEvent event, String address) {
        name = event.name;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return name + " (" + address + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Client) {
            Client client = (Client) obj;
            return toString().equals(client.toString());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
