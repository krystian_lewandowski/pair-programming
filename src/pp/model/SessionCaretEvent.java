package pp.model;

import java.awt.*;

/**
 * Created by citan on 20.06.2016.
 */
public class SessionCaretEvent extends SessionDocumentEvent {
    public Point oldPosition;
    public Point newPosition;
}
