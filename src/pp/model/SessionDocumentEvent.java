package pp.model;

import com.intellij.openapi.util.TextRange;

/**
 * Created by citan on 22.06.2016.
 */
public class SessionDocumentEvent extends SessionEvent {
    public int documentHashCode;
    public String documentPath;


    public TextRange[] oldRanges;
    public TextRange[] newRanges;
}
